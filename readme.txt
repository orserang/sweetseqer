How to run SweetSEQer:
------------------------------------

usage: ./SweetSEQer.py <epsilon> <lambda> <p> <g> <glycan_min_m/z> <print_text_output> <plot_results> <MGF_file_1> [MGF_file_2...]
	epsilon: maximum absolute error between predicted and actual peak (in m/z)
	lambda: minimum peak intensity (relative to maximum peak intensity)
	p: minimum length of inferred peptide sequence
	g: minimum size of inferred glycan graph (including isotope peaks, not shown in graph)
	tau: minimum m/z value to include in glycan
	print_text_output: either 0 (doesn't print text results) or 1 (prints text results for matches)
	plot_results: either 0 (doesn't plot results) or 1 (interactively plots each matching spectrum)
	MGF_file_1 [MGF_file_2...]: paths to an MGF files to process

Example:
	python src/SweetSEQer.py 0.025 0.01 0 10 100 1 1 data/120810_JF_HNU142_5.mgf 

Requirements:
------------------------------------
	SweetSEQer is written in python. In addition to Python,
	install the following packages:
	
		networkx
		pylab
		numpy

