class spectrum(dict):
    def __init__(self, line_generator, percent_peak_threshold):
        self.percent_peak_threshold = percent_peak_threshold
        # note: this doesn't verify the integrity of the file (via
        # finite state machine)
        for line in line_generator:
            # remove the newline
            line = line[:-1]
            if len(line.split()) == 0:
                continue
            if line.split()[0] == '#':
                continue
            elif line.startswith('BEGIN IONS'):
                continue
            elif line.startswith('END IONS'):
                break
            elif line.startswith('TITLE='):
                self.title = line[6:]
            elif line.startswith('SCANS='):
                self.scans = line[6:]
            elif line.startswith('CHARGE='):
                self.charge = line[7:]
            elif line.startswith('PEPMASS='):
                self.pepmass = line[8:]
            elif line.startswith('RTINSECONDS='):
                self.rtinseconds = line[12:]
            else:
                mz_str, intensity_str = line.split()
                self[float(mz_str)] = float(intensity_str)

        if len(self) == 0:
            raise Exception('Empty spectrum loaded')
        self.init_spectrum();
    def init_spectrum(self):
        # remove peaks below threshold
        self.max_intensity = max(self.values())
        for mz, intensity in self.items():
            if intensity < self.percent_peak_threshold * self.max_intensity:
                del self[mz]
    def remove_peaks_below_mz(self, mz_threshold):
        for mz, intensity in sorted(self.items()):
            if mz >= mz_threshold:
                break
            del self[mz]
    
class MGF:
    def __init__(self, fname, percent_peak_threshold):
        self.percent_peak_threshold = percent_peak_threshold
        self.load(fname)
    def load(self, fname):
        self.all_spectra = []
        # make readlines into a generator so that lines disappear
        # as they are read
        line_gen = ( line for line in open(fname).readlines() )
        while True:
            try:
                next_spect = spectrum(line_gen, self.percent_peak_threshold)
            except Exception:
                break
            # the spectrum was loaded successfully
            self.all_spectra.append(next_spect)
        if len(self.all_spectra) == 0:
            raise Exception('MGF file ' + fname + ' does not include any spectra')
