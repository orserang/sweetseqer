#!/usr/bin/python

from DeNovo import *
from copy import deepcopy

def main(args):
   try:
        error_tolerance = float(args[1])
        peak_intensity_tolerance = float(args[2])
        min_peptide_length = int(args[3])
        min_glycan_length = int(args[4])
        min_mz_for_glycan = float(args[5])
        print_output = bool(int(args[6]))
        draw_figures = bool(int(args[7]))

        print 'You called SweetSEQer with these parameters:'
        print '\tepsilon =', error_tolerance
        print '\tlambda =', peak_intensity_tolerance
        print '\tp =', min_peptide_length
        print '\tg =', min_glycan_length
        print '\tSearching for glycans at or above', min_mz_for_glycan, 'm/z'
        if draw_figures:
            print '\tDrawing figures for spectra of note'
        print ''

        if print_output or draw_figures:
           print 'key: '
           print circle.encode('utf-8'), 'Hexose'
           print square.encode('utf-8'), 'HexNac'
           print diamond.encode('utf-8'), 'dHex'
           print triangle.encode('utf-8'), 'NeuAc'

        assert(min_peptide_length >= 0 and min_glycan_length > 0)

        # go through each MGF file
        for mgf_fname in args[8:]:
            print 'Processing...', mgf_fname
            my_mgf = MGF(mgf_fname, peak_intensity_tolerance)

            # for each spectrum in the MGF file
            for spect in my_mgf.all_spectra:
                print '\tspectrum', spect.title,

                glyco_spectrum = deepcopy(spect)
                glyco_spectrum.remove_peaks_below_mz(min_mz_for_glycan)
                gn = GlycanDeNovo(glyco_spectrum, error_tolerance)
                pn = PeptideDeNovo(spect, error_tolerance)

                # test whether the quality of the peptide and glycan results are good enough

                # also, restrict to spectra with a depth of at least 2
                # (cannot be a star topology graph, e.g. A --> B, A
                # --> C, A --> D, A --> E...)
##                if len(gn.best_graph) > min_glycan_length and len(PeptideDeNovo.longest_path(gn.best_graph)) > 2 and len(pn.best_graph) > min_peptide_length:


                # note: no longer checks for star topology
                if gn.size() > min_glycan_length and len(pn.best_graph) > min_peptide_length:
                    # note: you can place any additional filters in here:
                    # for example, test that the first peak in the
                    # glycan_graph is close to a certain distance of some
                    # peak in the peptide graph. or check that the glycan
                    # graph forks at least once

                    # output the spectrum title, predicted peptide
                    # sequence, predicted glycan tree, and draws the
                    # spectrum
                    print 'matches'

                    if print_output:
                        print 'Path consistent with peptide sequence (or reverse sequence):'
                        pn.display()
                        print 'Tree consistent with glycan graph:'
                        gn.display()

                    if draw_figures:
                        draw_annotated_spectrum(spect, gn, pn)
                        draw_glycan_graph(gn)

                        print 'm/z range:', gn.start_node, gn.end_node

                        raw_input('[press return to continue]')
                        print ''

                        # clear the figure
                        P.clf()

                        print 'continued searching...'
                else:
                    print ''
   except Exception as e:
       print 'usage: ./SweetSEQer.py <epsilon> <lambda> <p> <g> <glycan_min_m/z> <print_text_output> <plot_results> <MGF_file_1> [MGF_file_2...]'
       print '\tepsilon: maximum absolute error between predicted and actual peak (in m/z)'
       print '\tlambda: minimum peak intensity (relative to maximum peak intensity)'
       print '\tp: minimum length of inferred peptide sequence'
       print '\tg: minimum size of inferred glycan graph (including isotope peaks, not shown in graph)'
       print '\ttau: minimum m/z value to include in glycan'
       print '\tprint_text_output: either 0 (doesn\'t print text results) or 1 (prints text results for matches)'
       print '\tplot_results: either 0 (doesn\'t plot results) or 1 (interactively plots each matching spectrum)'
       print '\tMGF_file_1 [MGF_file_2...]: paths to an MGF files to process'
       print ''
       print 'Exception:', e

if __name__ == '__main__':
    main(sys.argv)
