import sys
import networkx as nx
import pylab as P
from MGF import *
import numpy as np

# unicode characters for actual shapes

# circle = u"\u25CF" # (Hexose)
# square = u'\u25A0' # (HexNac)
# diamond = u"\u25C7" # use white diamond to distinguish from rotated square (dHex)
# triangle = u"\u25B2" # (NeuAc)

# note: changed to work on windows
circle = 'H' # (Hexose)
square = 'N' # (HexNac)
diamond = "S" # use white diamond to distinguish from rotated square (dHex)
triangle = "F" # (NeuAc)

def draw_annotated_spectrum(spect, glycan_de_novo, peptide_de_novo):
    P.ion()
    P.figure(1)
    P.clf()
    draw_spectrum_series(spect.items(), 'black')
    draw_spectrum_series(spectrum_and_mzs_to_drawing_edges(peptide_de_novo.best_graph, spect), 'blue', lw=3)
    draw_spectrum_series(spectrum_and_mzs_to_drawing_edges(glycan_de_novo.best_graph, spect), 'red', lw=2)
    P.draw()

def draw_glycan_graph(glycan_de_novo):
    graph = glycan_de_novo.merged_isos_best_graph
    P.ion()
    fig = P.figure(2)
    P.clf()
    graph = graph.to_undirected()
    # add the m/z for the lowest m/z node

    pos = nx.spring_layout(graph)
#    nx.draw_networkx(graph, pos, node_color="white", with_labels=False)
    nx.draw_networkx_edges(graph, pos, node_color="white", with_labels=False)
    edges = graph.edges()
    nx.draw_networkx_edge_labels(graph, pos, edge_labels = dict( zip(edges, [ graph[e[0]][e[1]]['label'] for e in edges ]) ) )

    # plot the starting m/z in the corner and indicate the
    # starting node with a '*'
    P.text(pos[glycan_de_novo.start_node][0], pos[glycan_de_novo.start_node][1], '*')

    # add the text in the corner showing the minimum m/z peak in the chain
    ax1 = P.axes()
    P.text(0, ax1.get_ylim()[1]*1.1*0.95, '* ' + str(glycan_de_novo.start_node))

    P.ylim(ax1.get_ylim()[0], ax1.get_ylim()[1]*1.1)

    # turn off the axes
    ax1.axes.get_xaxis().set_visible(False)
    ax1.axes.get_yaxis().set_visible(False)

    P.draw()

def draw_spectrum_series(spect_series, color = 'black', lw=1):
    series = []
    for mz, intensity in sorted(spect_series):
        series.extend( [ (mz,0), (mz,intensity), (mz,0) ] )
    draw_paired_series(series, color, lw)

def draw_paired_series(series, color, lw):
    P.plot( [x for x,y in series], [y for x,y in series], color=color, lw=lw)

def spectrum_and_mzs_to_drawing_edges(dg, spect):
    edge_series = []
    for edge in dg.edges():
        edge_series.append( (edge[0], spect[ edge[0] ]) )
        edge_series.append( (edge[1], spect[ edge[1] ]) )
    # convert to set and then back to list to remove duplicates
    return list(set(edge_series))

def indent(depth):
    for i in xrange(depth):
        print '     ',

class DeNovo:
    def __init__(self, spect, max_distance, deltas, possible_charges = [1.0, 2.0, 3.0]):
        self.spect = spect
        self.max_distance = max_distance
        self.deltas = [ (d[0], d[1]+iso_count*1.003) for d in deltas for iso_count in (0,-1,1) ]
        self.possible_charges = possible_charges
        self.mzSorted = sorted(list(spect))

        # note: init_best_graph_and_charge_state calls functions
        # defined in derived classes; thus it would need to be
        # performed outside of the base constructor in C++ (the
        # virtual table of only the base class is available here in
        # C++).
        self.init_best_graph_and_charge_state()

        # initialize the minimum and maximum m/z values used
        self.start_node = min(list(self.merged_isos_best_graph) + [np.inf] )
        self.end_node = max(list(self.merged_isos_best_graph) + [-np.inf])
    def add_isoshift_edges(self, dg):
        # for all sibling nodes B, C where n --> B and n --> C, add
        # edges from:
        ## -B to all successors of C
        ### -C to all successors of B
        #### -all predecessors of B to C
        ##### -all predecessors of C to B
        # this ensures the nodes are treated properly (you can go in
        # one and come out the other)
        edges_to_add = []
        for a in dg:
            successors = list(dg[a])
            for sucB in successors:
                for sucC in successors:
                    if dg[a][sucB]['label'] == dg[a][sucC]['label']:
                        if sucB == sucC:
                            continue
                        # add edges for sucB and sucC
                        for desc in dg.successors(sucC):
                            edges_to_add.append( (sucB, desc, { 'label' : dg[sucC][desc]['label'] }))
                        for anc in dg.predecessors(sucC):
                            edges_to_add.append( (anc, sucB, { 'label' : dg[anc][sucC]['label'] }))
        for a in dg:
            predecessors = list(dg.predecessors(a))
            for predB in predecessors:
                for predC in predecessors:
                    if dg[predB][a]['label'] == dg[predC][a]['label']:
                        if predB == predC:
                            continue
                        # add edges for sucB and sucC
                        for desc in dg.successors(predC):
                            edges_to_add.append( (predB, desc, { 'label' : dg[predC][desc]['label'] }))
                        for anc in dg.predecessors(predC):
                            edges_to_add.append( (anc, predB, { 'label' : dg[anc][predC]['label'] }))
        for edge in edges_to_add:
            dg.add_edge(*edge)
    def peak_nearest_mz_helper(self, mz, low, high):
        # note: could be adapted to use a hash table and windows for
        # greater speed (particularly when error tolerance is low);
        # also, all peaks within the error tolerance could be found,
        # not simply the closest.
        if low == high:
            return self.mzSorted[low]
        if low+1 == high:
            # arbitrarily resolves ties
            if abs(self.mzSorted[low] - mz) < abs(self.mzSorted[high] - mz):
                return self.mzSorted[low]
            else:
                return self.mzSorted[high]
        mid = low + (high - low + 1) / 2
        if mz < self.mzSorted[mid]:
            high = mid
        elif mz > self.mzSorted[mid]:
            low = mid
        else:
            # mz == self.mzSorted[mid]
            return mz
        return self.peak_nearest_mz_helper(mz, low, high)
    def peak_nearest_mz(self,mz):
        # self.mzSorted is composed of sorted, unique elements
        assert(len(self.mzSorted) > 0)
        if len(self.mzSorted) == 1:
            return self.mzSorted[0]
        low, high = 0, len(self.mzSorted)-1
        return self.peak_nearest_mz_helper(mz, low, high)
    def build_graph(self, charge):
        dg = nx.DiGraph()
        for mz in self.mzSorted:
            for lbl, val in self.deltas:
                change = val / charge
                neighborMz = self.peak_nearest_mz(mz + change)
                distance = abs(mz+change - neighborMz)
                # normalize the max m/z error with the charge-- doubly
                # charged should be allowed twice the error
#                if distance < charge*self.max_distance and neighborMz > mz:
                if distance < self.max_distance and neighborMz > mz:
                    dg.add_edge( mz, neighborMz, {'label':lbl} )
        self.add_isoshift_edges(dg)
        return dg
    def init_best_graph_and_charge_state(self):
        results_for_all_charges = []
        for charge in self.possible_charges:
            dg = self.build_graph(charge)
            best_graph_for_charge = self.maximal_directed_connected_graph(dg)
#            print '\t',charge, len(best_graph_for_charge)
            results_for_all_charges.append( (best_graph_for_charge, charge) )
        self.best_graph, self.best_charge_state = max( [ (len(graph), (graph, charge)) for graph, charge in results_for_all_charges ] )[1]
        self.merged_isos_best_graph = self.merge_isos(self.best_graph)
    def merge_isos(self, dg):
        merged_nodes = set()
        for a in dg:
            for b in dg:
                if a == b:
                    continue
                if self.has_merge_nodes(dg, merged_nodes, a, b):
                    continue
        
        # copy dg to result
        unlabeled_edges = dg.edges()
        result = nx.DiGraph()
        result.add_nodes_from(dg)
        result.add_edges_from([ (a, b, dg[a][b]) for a, b in unlabeled_edges ])
        for a, b in merged_nodes:
            # check to make sure the nodes have not been removed from
            # the graph yet
            if a not in result or b not in result:
                continue
            # merge the nodes (a gets all of b's edges)
            for pred in result.predecessors(b):
                result.add_edge(pred, a, result[pred][b])
            for succ in result.successors(b):
                result.add_edge(a, succ, result[b][succ])
            result.remove_node(b)

        return result
                
    def has_merge_nodes(self, dg, merged_nodes, a, b):
        for p in dg.predecessors(a):
            for q in dg.predecessors(b):
                if p == q or (p,q) in merged_nodes:
                    # p and q effectively share a parent
                    if dg[p][a]['label'] == dg[q][b]['label']:
                        merged_nodes.add( (a,b) )
                        merged_nodes.add( (b,a) )
                        return True
        for p in dg.successors(a):
            for q in dg.successors(b):
                if p == q or (p,q) in merged_nodes:
                    # p and q effectively share a parent
                    if dg[a][p]['label'] == dg[b][q]['label']:
                        merged_nodes.add( (a,b) )
                        merged_nodes.add( (b,a) )
                        return True
        return False
        
    def maximal_directed_connected_graph(self, dg):
        raise Exception('pure virtual')
    def size(self):
        return len(self.best_graph)

class GraphDeNovo(DeNovo):
    def non_isoshift_size(self):
        return len(self.merged_isos_best_graph)
    # finds largest connected directed subgraph
    def maximal_directed_connected_graph(self, dg):
        # find connected components with dynamic programming
        # (performed by networkx package)
        connected_graphs = list(nx.connected_component_subgraphs(dg.to_undirected()))
        if len(connected_graphs) == 0:
            return nx.DiGraph()
        best_undirected_graph = max( [ (len(g),g) for g in connected_graphs ] )[1]
        return nx.subgraph(dg, best_undirected_graph.nodes())

    def display(self):
        print 'charge state:', self.best_charge_state
        print 'graph (with merged isotope peaks) is in m/z range:', self.start_node, self.end_node
        self.display_graph()

    def display_graph_helper(self, root, visited_nodes, depth = 0):
        print root
        visited_nodes.add(root)
        for n in self.best_graph.successors(root):
            if n not in visited_nodes:
                indent(depth+1)
                print '+', self.best_graph[root][n]['label'].encode('utf-8'), '-->',
                self.display_graph_helper(n, visited_nodes, depth+1)
            else:
                indent(depth+1)
                print '+', self.best_graph[root][n]['label'].encode('utf-8'), n

    def display_graph(self):
        visited_nodes = set()
        root = None
        # can also print the graph where redundant paths from isotopic
        # shifts are merged by commenting out the second line and
        # commenting the first:
        graph = self.best_graph
#        graph = self.merged_isos_best_graph
        for n in graph:
            if len(graph.predecessors(n)) == 0:
                root = n
                self.display_graph_helper(root, visited_nodes)
        print ''

class PathDeNovo(DeNovo):
    def display(self):
        print 'charge state:', self.best_charge_state
        self.display_graph()
    def display_graph(self):
        for a,b in sorted(self.best_graph.edges()):
            print a, b, self.best_graph[a][b]['label']
        print ''

    # routines to find longest path
    def maximal_directed_connected_graph(self, dg):
        return PathDeNovo.longest_path(dg)

    @classmethod
    def longest_path(cls, dg):
        # use dynamic programming to force the longest path (not
        # simply the largest connected directed subgraph)
        if len(dg.edges()) == 0:
            return nx.DiGraph()
        node_to_len_of_longest_path_ending = {}
        # initialize the cache by querying every node
        longest_path_len_and_node = max([ (cls.len_of_longest_path_ending(dg, n, node_to_len_of_longest_path_ending),n) for n in dg ])
        
        best_len, tail_of_longest_path = longest_path_len_and_node

        edges = []
        while True:
            preds = dg.predecessors(tail_of_longest_path)
            if len(preds) == 0:
                break
            best_len -= 1
            for n in preds:
                if node_to_len_of_longest_path_ending[n] == best_len:
                    edges.append( (n, tail_of_longest_path) )
                    break
            tail_of_longest_path = n
        result = nx.DiGraph()
        for a, b in edges[::-1]:
            result.add_edge(a, b, dg[a][b])
        return result
    @classmethod
    def len_of_longest_path_ending(cls, dg, n, node_to_len_of_longest_path_ending):
        # compute the longest path ending at each node using dynamic
        # programming (sloppily implemented using a simple dictionary
        # cache)
        if n in node_to_len_of_longest_path_ending:
            return node_to_len_of_longest_path_ending[n]
        node_to_len_of_longest_path_ending[n] = 0
        preds = dg.predecessors(n)
        for m in preds:
            node_to_len_of_longest_path_ending[n] = max(node_to_len_of_longest_path_ending[n], 1 + cls.len_of_longest_path_ending(dg, m, node_to_len_of_longest_path_ending) )
        return node_to_len_of_longest_path_ending[n]


class GlycanDeNovo(GraphDeNovo):
    def __init__(self, spect, max_distance):
        iso_shift_constant = 1.003
#        GraphDeNovo.__init__(self, spect, max_distance, [ ('Hexose', 162.0528), ('HexNac', 203.0794), ('dHex', 146.0579), ('NeuAc', 291.0954) ])
        GraphDeNovo.__init__(self, spect, max_distance, [ (circle, 162.0528), (square, 203.0794), (triangle, 146.0579), (diamond, 291.0954) ])
    
class PeptideDeNovo(PathDeNovo):
    def __init__(self, spect, max_distance):
        PathDeNovo.__init__(self, spect, max_distance, [ ('A', 71.03711), ('R', 156.10111), ('N', 114.04293), ('D', 115.02694), ('C', 103.00919), ('E', 129.04259), ('Q', 128.05858), ('G', 57.0519), ('H', 137.05891), ('I/L', 113.08406), ('K', 128.09496), ('M', 131.04049), ('F', 147.06841), ('P', 97.05276), ('S', 87.03203), ('T', 101.04768), ('W', 186.07931), ('Y', 163.06333), ('V', 99.06841) ])

